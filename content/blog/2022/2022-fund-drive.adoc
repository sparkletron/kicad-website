+++
title = "2022 End of Year Fund Drive"
date = "2022-11-22"
draft = false
"blog/categories" = [
    "News"
]
+++

It's that time of year to prepare for our annual end of year funding drive.  With
the next stable release (7) just around the corner (January 31st, 2023), it's time
to start thinking about building version 8.  Our supporters' contributions have done
more to build the features of version 7 than any other source of funding.  With your
help, we can make version 8 better than ever!  Our year
end donation campaign with begin December 1st and run through January 15th.  Make
sure to mark you calendars.

KiCad is excited to announce that we will have mutiple sponsors for the funding
drive matching your donations to the KiCad project.
https://www.kipro-pcb.com/[KiCad Services Corporation], https://www.digikey.com/[Digi-Key], the
https://www.raspberrypi.org/[Raspberry Pi Foundation], and
https://www.pcbway.com/[PCBWay] have all agreed to match donations during the
campaign.  Keep an eye out for each sponsor's donation match dates on the KiCad
website +++<a href="https://donate.kicad.org" data-bs-toggle="modal"
data-bs-target="#donate-modal">donation page</a>+++ to make your contribution to
KiCad go even further.

If your company would like to sponsor matching donations during the 2022 funding drive,
please contact us at mailto:sponsorship@kicad.org[sponsorship@kicad.org], we have a few
dates remaining available for sponsorship this year!
