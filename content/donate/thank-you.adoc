+++
title = "Thank you for supporting KiCad!"
date = "2021-03-26"
aliases = [ "/thank-you/" ]
headless = true
summary = "Thank you for your donation"
+++

You are a vital part of the KiCad community.

#### KiCad is user-driven and user-supported. Thanks to you, in the past year, we have been able to do the following:

{{< aboutlink "/img/donations/2022_flyer.png" "/img/donations/2022_flyer.png" >}}