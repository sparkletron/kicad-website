+++
title = "Container / Docker Images"
distro = "Docker"
summary = "Obtaining docker images"
iconhtml = "<div class='fl-docker'></div>"
weight = 100
+++
:dist: Docker

== Basic Information

KiCad Docker images are available and intended for usage of the `kicad-cli` binary which allows
command line based tooling to do things like exporting SVGs, Gerbers and more from KiCad file formats.

The official images are available at:

https://hub.docker.com/r/kicad/kicad

along with a mirror at

https://github.com/orgs/KiCad/packages/container/package/kicad


NOTE: Using the GUI environment via Docker is not supported. Any bugs must be reproduced on a supported operating system.
